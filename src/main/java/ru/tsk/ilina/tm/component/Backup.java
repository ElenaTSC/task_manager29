package ru.tsk.ilina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class Backup extends Thread {

    @NotNull
    private static final String COMMAND_SAVE = "backup-save";
    @NotNull
    private static final String COMMAND_LOAD = "backup-load";
    @NotNull
    private static final int INTERVAL = 30;
    @NotNull
    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.runCommand(COMMAND_SAVE);
    }

    public void load() {
        bootstrap.runCommand(COMMAND_LOAD);
    }
}
