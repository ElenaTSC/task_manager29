package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands";
    }

    @NotNull
    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandService().getCommand()) {
            System.out.println(command.toString());
        }
    }

}
