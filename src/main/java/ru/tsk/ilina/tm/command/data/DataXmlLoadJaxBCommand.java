package ru.tsk.ilina.tm.command.data;

import com.sun.jmx.remote.internal.Unmarshal;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-xml-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Load data in Xml with JaxB library";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshal = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshal.unmarshal(file);
        setDomain(domain);
    }

}
