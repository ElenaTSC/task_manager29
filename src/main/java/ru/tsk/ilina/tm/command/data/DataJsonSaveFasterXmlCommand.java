package ru.tsk.ilina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-json-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Save data in JSON with FasterXml library";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTER_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
