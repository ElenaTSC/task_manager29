package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.model.Command;

public final class ShowArgumentCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "-arg";
    }

    @NotNull
    @Override
    public String arg() {
        return "Display list arguments.";
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandService().getArguments()) {
            System.out.println(command.arg().toString());
        }
    }

}
