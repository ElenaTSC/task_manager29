package ru.tsk.ilina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-yaml-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Load data in YAML with FasterXml library";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_YAM)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

}
