package ru.tsk.ilina.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied");
    }

    public AccessDeniedException(@NotNull final String message) {
        super("Error! " + message + " access denied");
    }

}
