package ru.tsk.ilina.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is not correct");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(@NotNull final String message) {
        super("Error! " + message + " the value is not number");
    }

}
