package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty");
    }

    public EmptyEmailException(@NotNull final String message) {
        super("Error! " + message + " email is empty");
    }

}
