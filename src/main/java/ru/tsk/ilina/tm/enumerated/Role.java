package ru.tsk.ilina.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
@Getter
public enum Role {

    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

}
