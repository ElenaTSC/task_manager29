package ru.tsk.ilina.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";
    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";
    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";
    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "ILINA ELENA";
    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY = "application.developer";
    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";
    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";
    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    private final Properties properties = new Properties();

    private String getValue(final String name, final String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultValue);
    }

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @Override
    public @NotNull String getApplicationVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public @NotNull String getEmail() {
        return getValue(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloper() {
        return getValue(APPLICATION_DEVELOPER_KEY, APPLICATION_DEVELOPER_DEFAULT);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        return Integer.parseInt(getValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }
}
