package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.AbstractBusinessEntity;

public interface IAbstractBusinessRepository<E extends AbstractBusinessEntity> extends IAbstractOwnerRepository<E> {

    E removeByName(String userId, String name);

    E findByName(String userId, String name);

    E startByID(String userId, String id);

    E startByIndex(String userId, Integer index);

    E startByName(String userId, String name);

    E changeStatusByID(String userId, String id, Status status);

    E changeStatusByIndex(String userId, Integer index, Status status);

    E changeStatusByName(String userId, String name, Status status);

    E finishByID(String userId, String id);

    E finishByIndex(String userId, Integer index);

    E finishByName(String userId, String name);

}
