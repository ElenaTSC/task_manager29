package ru.tsk.ilina.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    Date getCreatedDate();

    void setCreatedDate(Date createdDate);
}
