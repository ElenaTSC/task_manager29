package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.AbstractBusinessEntity;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IAbstractOwnerService<E extends AbstractOwnerEntity> extends IAbstractService<E> {

    void add(String userId, E entity);

    void remove(String userId, E entity);

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findByID(String userId, String id);

    E removeByID(String userId, String id);

}
