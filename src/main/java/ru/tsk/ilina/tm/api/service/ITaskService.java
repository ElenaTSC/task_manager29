package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.model.User;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IAbstractBusinessService<Task> {

    void create(String userId, String name, String description);

}
