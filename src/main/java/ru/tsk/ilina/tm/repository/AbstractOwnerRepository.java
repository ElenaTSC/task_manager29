package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractOwnerRepository;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor
public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IAbstractOwnerRepository<E> {

    @NotNull
    public static Predicate<AbstractOwnerEntity> predicateById(@NotNull final String userId) {
        return o -> userId.equals(o.getUserId());
    }

    @Nullable
    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        entities.stream().filter(predicateById(userId)).collect(Collectors.toList()).remove(entity);
    }

    @NotNull
    @Override
    public void clear(@NotNull final String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getId()));
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) {
        return entities.stream().filter(predicateById(userId)).collect(Collectors.toList()).size();
    }

    @NotNull
    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream().filter(predicateById(userId)).collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.stream().filter(predicateById(userId)).sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<E> entity = findAll(userId);
        return entity.get(index);
    }

    @Nullable
    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return Optional.ofNullable(findByID(userId, id)).isPresent();
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

    @Nullable
    @Override
    public E findByID(@NotNull final String userId, @NotNull final String id) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && id.equals(o.getId())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E removeByID(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByID(userId, id));
        entity.ifPresent(entities::remove);
        return entity.get();
    }

}
